angular.
    module('app.register', [])
    .controller('RegisterCtrl', registerCtrl);

registerCtrl.$inject = ['registerService', '$state'];
function registerCtrl(registerService, $state){
    var vm = this;

    vm.user = {};

    //actions
    vm.signup = signup;

    function signup(){
        registerService.register(vm.user).then(function(success){
            console.log(success);
            $state.go('login');
        }, function(err){
            console.log(err);
        }).then(function(){
            //clean view
            vm.user = {};
        })
    }

}