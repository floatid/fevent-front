angular
    .module('app')
    .directive('fvntHeader', fvntHeader);

fvntHeader.$inject = []
function fvntHeader(){
    var directive = {
        restrict: 'AE',
        template: require('./fvnt-header.tpl.html'),
        scope: '=',
        link: link,
        controller: controller,
        controllerAs: 'headerCtrl'
    }

    controller.$inject = ['$mdSidenav'];
    function controller( $mdSidenav){
        var vm = this;

        vm.toggleSideNav = function(){
            $mdSidenav('left').toggle();
        }

    }

    function link(scope, elem, attr){
        var scopes = scope;
        console.log(scope);
    }
    return directive;
}