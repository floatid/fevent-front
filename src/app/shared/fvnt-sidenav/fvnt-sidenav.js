angular
    .module('app')
    .directive('fvntSideNav', fvntSideNav)

fvntSideNav.$inject = [];
function fvntSideNav(){

    var directive = {
        restrict: 'AE',
        template: require('./fvnt-sidenav.tpl.html'),
        scope: '=',
        link: link,
        controller: controller,
        controllerAs: 'sideNavCtrl'
    };

    controller.$inject = ['$scope', '$mdSidenav'];
    function controller($scope, $mdSidenav){
        var vm = this;

        vm.toggle = function(){
            $mdSidenav('left').toggle()
        }

    }

    function link(scope, elem, attrs){

    }


    return directive;
}
