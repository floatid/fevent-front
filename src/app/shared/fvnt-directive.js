angular
    .module('app')
    .directive('fvntDirective', fvntDirective);

fvntDirective.$inject = ['$compile']
function fvntDirective($compile){

    var directive = {
        template: '',
        link: function(scope, el, attr){
            el.append($compile('<' + attr.fvntDirective + '>')(scope));
        }
    }

    return directive;
}