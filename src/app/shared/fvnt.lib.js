angular
    .module('app')
    .factory('fvntLib', fvntLib);

fvntLib.$inject = []
function fvntLib(){

    const logoFloatId = require('../../img/floatid.png');
    const logoFvnt = require('../../img/floatid.png');

    var factory = {
        logoFloatId: logoFloatId,
        logoFvnt: logoFvnt,
    }

    return factory;
}