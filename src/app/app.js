var angular = require('angular');
//Require Vendors
require('angular-ui-router');
require('ng-dialog');
//Require other modules
require('./main');
require('./stores');
require('./services');
//require('./templates');
require('./login');
require('./register');
require('./dashboard');

angular
    .module('app', [
        /* Vendors */
        'ui.router',
        'ngDialog',
        'dndLists',
        'ngMaterial',
        'mdSteppers',
        /* App SubModules */
        'app.stores',
        'app.services',
        'app.login',
        'app.main',
        'app.register',
        'app.dashboard'
    ])
    .run(runApp);

require('./shared');
require('./vendors');
//Require app config and routes
if(process.env.DEBUG){
    require('./app.constant');
}else{
    require('./app.constant.prod');
}
require('./app.routes');
require('./app.config');
//Require Directive
//require css
require('../css/index.scss');
//require css vendors
require('angular-material/angular-material.scss');
require('material-steppers/lib/material-steppers.min.css');

runApp.$inject = ['$rootScope', '$state'];
function runApp ($rootScope, $state) {
    $rootScope.$state = $state;
    $rootScope.$on('$stateChangeStart', function (event, next, current) {

    });
};
