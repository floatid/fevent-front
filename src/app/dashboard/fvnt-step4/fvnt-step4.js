angular.module('app.dashboard').
    directive('fvntStep4', fvntStep4)


function fvntStep4(){
    var directive = {
        restrict: 'AEC',
        template: require('./fvnt-step4.tpl.html'),
        scope: '=',
        controller: controller,
        controllerAs: 'step4',
        link: link
    }

    controller.$inject = ['$scope', 'stepperDelegate']
    function controller($scope, stepperDelegate){
        var vm = this;

        vm.products = stepperDelegate.fullEvent.products;


    }

    function link(scope, elem, attrs){

    }


    return directive;
}
