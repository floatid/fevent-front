angular
    .module('app.dashboard')
    .controller('StepperCtrl', stepperCtrl);

stepperCtrl.$inject = ['$timeout', '$mdMedia', '$state', 'stepperDelegate', '$element'];
function stepperCtrl($timeout, $mdMedia, $state, stepperDelegate, $element){
    var vm = this;

    $element.ready(function(){
        stepperDelegate.init('dashboard-stepper');
    });


    vm.steps = [
        {
            title: 'Datos principales',
            templateName: 'fvnt-step1'
        },
        {
            title: 'Funcionalidades',
            templateName: 'fvnt-step2'
        },
        {
            title: 'Interfaz',
            templateName: 'fvnt-step3'
        },
        {
            title: 'Productos',
            templateName: 'fvnt-step4'
        },
        {
            title: 'Pago',
            templateName: 'fvnt-step5'
        },
        {
            title: 'Fevnt Ready',
            templateName: 'fvnt-step6'
        },
    ];

    //public functions

    vm.lastOne = stepperDelegate.isLastOne;

    vm.isVertical = function(){
        if($mdMedia('xs')){
            return true;
        }
        return false;
    };

    vm.next = function (){
        stepperDelegate.next(goToFinish);
    };

    vm.back = function(){
        stepperDelegate.back(goToFinish);
    };

    //private functions

    function goToFinish(){
        $state.go('dashboard');
    }
}