angular
    .module('app.dashboard')
    .factory('stepperDelegate', stepperDelegate)
stepperDelegate.$inject = ['$mdStepper']
function stepperDelegate($mdStepper){
    var fullEvent = {
        form: {},
        functionalities: [],
        products: [
            //this must  be get from db
            {name: "product1"},
            {name: "product2"},
            {name: "product3"},
            {name: "product4"},
            {name: "product5"},
        ],
        paymentForm: {}
    };
    var stepperInstance;
    var nextCallback = function(){};

    var delegate = {
        init: init,
        next: next,
        back: back,
        setNextCallback: setNextCallback,
        isLastOne: isLastOne,
        fullEvent: fullEvent

    }

    function setNextCallback(fn){
        nextCallback = fn;
    }

    function init(name){
        stepperInstance = $mdStepper(name);
        if(!stepperInstance){
            return false;
        }
        return true;
    }

    function next(callback){
        if((isLastOne() | !stepperInstance.next()) && callback){
            callback()
        }
    }

    function back(callback){
        if(!stepperInstance.back() && callback){
            callback()
        }
    }

    function isLastOne(){
        return stepperInstance && stepperInstance.steps.length - 1 === stepperInstance.currentStep;
    }

    return delegate;
}