angular.module('app.dashboard').
    directive('fvntStep2', fvntStep2)


function fvntStep2(){
    var directive = {
        restrict: 'AEC',
        template: require('./fvnt-step2.tpl.html'),
        scope: '=',
        controller: controller,
        controllerAs: 'step2',
        link: link
    }

    controller.$inject = ['$scope', 'stepperDelegate']
    function controller($scope, stepperDelegate){
        var vm = this;

        vm.lists = {
            fevntList: {
                title: "Nuestras opciones",
                funcs: [
                    {name: "func1"},
                    {name: "func2"},
                    {name: "func3"}
                ]
            },
            clientList: {
                title: "Las tuyas",
                funcs: stepperDelegate.fullEvent.functionalities
            }
        };


    }

    function link(scope, elem, attrs){

    }


    return directive;
}
