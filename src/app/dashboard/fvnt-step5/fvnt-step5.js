angular.module('app.dashboard').
    directive('fvntStep5', fvntStep5)


function fvntStep5(){
    var directive = {
        restrict: 'AEC',
        template: require('./fvnt-step5.tpl.html'),
        scope: '=',
        controller: controller,
        controllerAs: 'step5',
        link: link
    }

    controller.$inject = ['$scope', 'stepperDelegate']
    function controller($scope, stepperDelegate){
        var vm = this;

        vm.titles = ['Mr', 'Ms'];
        vm.paymentForm = stepperDelegate.fullEvent.paymentForm;


    }

    function link(scope, elem, attrs){

    }


    return directive;
}
