angular
    .module('app.dashboard')
    .directive('fvntStep1', fvntStep1);

fvntStep1.$inject = [];
function fvntStep1(){
    var directive = {
        restrict: 'AEC',
        template: require('./fvnt-step1.tpl.html'),
        scope: '=',
        controller: controller,
        controllerAs: 'step1',
        link: link
    }

    controller.$inject = ['$scope', 'stepperDelegate', 'NgMap']
    function controller($scope, stepperDelegate, NgMap){

        var map;
        var vm= this;

        vm.categories = [
            {
                id: 1,
                name: "Party"
            },
            {
                id: 2,
                name: "Party"
            },
            {
                id: 3,
                name: "Party"
            },
            {
                id: 4,
                name: "Party"
            },
            {
                id: 5,
                name: "Party"
            }
        ]

        vm.form = stepperDelegate.fullEvent.form;

        vm.mapInit = function(){
            NgMap.getMap().then(function(resultMap){
                map = resultMap;
                console.log(map);
            })
        }


    }

    function link(scope, elem, attr){
        var e = scope;
    }

    return directive;
}