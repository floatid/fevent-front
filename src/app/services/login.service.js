angular
    .module('app.services')
    .factory('loginService', loginService);

loginService.$inject = ['APP_CONFIG', '$q', '$http']
function loginService(APP_CONFIG, $q, $http){
    var service = {
        login: login,
    }

    function login(payload){
        return $http({
            method:'POST',
            url: APP_CONFIG.API_URL + "/auth/login",
            data: payload
        }).then(function(res){
            //store user
            var data = res.data;
            if(data.result.ok === 'ko'){
                return $q.reject(data.result.msg || data.result.ok)
            }
            return $q.resolve(data);
        }, function(err){
            //parse error
            return $q.reject(err);
        })
    }

    return service;
}
