angular
    .module('app.services')
    .factory('registerService', registerService);

registerService.$inject = ['APP_CONFIG', '$q', '$http']
function registerService(APP_CONFIG, $q, $http){
    var service = {
        register: register,
    }

    function register(user){
        return $http({
            method:'POST',
            url: APP_CONFIG.API_URL + "/users",
            data: user
        }).then(function(res){
            //store user
            var data = res.data;
            if(data.result.ok === 'ko'){
                return $q.reject(data.result.msg || data.result.ok)
            }
            return $q.resolve(data);
        }, function(err){
            //parse error
            return $q.reject(err);
        })
    }

    return service;
}
