angular
    .module('app')
    .constant('HTTP_TIMEOUT_LIMIT', 30000)
    .constant('APP_CONFIG', {
        'API_URL': 'http://localhost:1210/api'
    });