angular.module('app.login', [])
    .controller('LoginCtrl', loginCtrl);

loginCtrl.$inject = ['loginService', '$state', 'fvntLib'];
function loginCtrl(loginService, $state, fvntLib){
    var vm = this;

    //Parameters
    vm.payload = {};
    vm.img = fvntLib.logoFloatId;

    //actions
    vm.login = login;

    function login(){
        loginService.login(vm.payload).then(function(success){
            console.log(success);
            $state.go('dashboard');
        }, function(err){
            console.log(err);
            alert(err);
        })
    }
}