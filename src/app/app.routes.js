angular
    .module('app')
    .config(router);

router.$inject = [
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider'
];

function router($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
        .state('login', {
            url: "/login",
            template: require('./login/login.tpl.html'),
            controller: 'LoginCtrl',
            controllerAs: 'loginCtrl',
        })
        .state('register', {
            url: "/register",
            template: require('./register/register.tpl.html'),
            controller: 'RegisterCtrl',
            controllerAs: 'registerCtrl',
        })
        .state('dashboard', {
            url: "/dashboard",
            template: require('./dashboard/dashboard.tpl.html'),
            controller: 'DashboardCtrl',
            controllerAs: 'dashboardCtrl',
        })
        .state('dashboard.stepper', {
            url: "/stepper",
            template: require('./dashboard/dashboard.stepper/stepper.tpl.html'),
            controller: 'StepperCtrl',
            controllerAs: 'stepperCtrl'
        })


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');

    //$locationProvider.html5Mode(true);

};
