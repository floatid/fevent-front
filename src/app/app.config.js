angular
    .module('app')
    .factory('commonInterceptor', commonInterceptor)
    .config(providersWhiteList)
    .config(configInterceptors)
    .config(themeProvider);


configInterceptors.$inject = ['$httpProvider'];
function configInterceptors($httpProvider) {
    $httpProvider.interceptors.push('commonInterceptor');
};

providersWhiteList.$inject = ['$compileProvider'];
function providersWhiteList($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel|blob):|data:image\//);
};

themeProvider.$inject = ['$mdThemingProvider'];
function themeProvider($mdThemingProvider){
    $mdThemingProvider.definePalette('feventPrimary', {
        '50': 'ffebee',
        '100': 'ffcdd2',
        '200': 'ef9a9a',
        '300': 'e57373',
        '400': 'ef5350',
        '500': 'f44336',
        '600': 'e53935',
        '700': 'd32f2f',
        '800': 'c62828',
        '900': 'b71c1c',
        'A100': 'ff8a80',
        'A200': 'ff5252',
        'A400': 'ff1744',
        'A700': 'd50000',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                            // on this palette should be dark or light

        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
            '200', '300', '400', 'A100'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
    });

    $mdThemingProvider.theme('default')
        .primaryPalette('teal',{
            'hue-1': '200',
            'hue-2': '600',
            'hue-3': 'A100'
        })
        //#19B394
         //#F9AB58
         //#23C5C8
        .accentPalette('orange');
}



commonInterceptor.$inject = ['$q', 'HTTP_TIMEOUT_LIMIT', '$injector', '$rootScope'];
function commonInterceptor($q, HTTP_TIMEOUT_LIMIT, $injector, $rootScope) {
    return {
        'request': function (config) {
            //$rootScope.loading = false;
            config.timeout = HTTP_TIMEOUT_LIMIT || 10000;
            config.headers.test = 'test';
            return config;
        },
        'response': function (response) {
            //$rootScope.loading = true;
            return $q.resolve(response);
        },
        responseError: function (response) {
            //$rootScope.loading = false;
            //var errorHandler = $injector.get('nsaErrorHandler');
            //return errorHandler.handleResponseError(response);
            return $q.reject(response);
        },
        requestError: function(config){
            return config;
        }
    };
};
