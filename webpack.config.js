const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const clean = require('clean-webpack-plugin');
const PROD = JSON.parse(process.env.PROD_ENV || '0');

module.exports = {
    entry: {
        app: './src/app/app.js',
        vendor: [
            'angular',
            'jquery',
            'angular-drag-and-drop-lists',
            'angular-animate',
            'angular-aria',
            'angular-material',
            'material-steppers',
            'ngmap'
        ]
    },
    output: {
        path: './dist',
        filename: '[name].bundle.js',
        chunkFilename: "[id].bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style','css!sass')
            },
            {
                test: /\.css/,
                loader: ExtractTextPlugin.extract('style','css')
            },
            {
                test: /\.tpl\.html$/,
                loader: 'raw-loader?root=src/img'
            },
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
                loader: 'file-loader'
            },
            {
                test: /bootstrap-sass\/assets\/javascripts\//,
                loader: 'imports?jQuery=jquery'
            }
        ]

    },
    plugins: PROD ? [
        new webpack.optimize.UglifyJsPlugin(),
        new clean(['dist']),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: true
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor' // Specify the common bundle's name.
        }),
        new ExtractTextPlugin('app.css'),
    ] : [
        new clean(['dist']),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: true
        }),

        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor' // Specify the common bundle's name.
        }),
        new ExtractTextPlugin('app.css'),
        new webpack.EnvironmentPlugin(['DEBUG'])
    ]

};